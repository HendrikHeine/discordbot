import os
import random
import requests
import json


class addon:
    def __init__(self) -> None:
        self.__activitys = [
            "Geh doch mal nach Draußen",
            "Mach deine Hausaufgaben XD",
            "Toaste doch mal ne Banane",
            "Geh saufen :D ?saufen wenn du hilfe brauchst",
            "HAHAHAHA hol dir mal Freunde du looser XD",
            "Nerv mich nicht...",
            "Spiel doch mal im Sandkasten...",
            "Lern Chinesisch...",
            "Koch dein Lieblingsgericht :D",
            "Putz deine Wohnung, es stinkt langsam..."
        ]

        self.__drinks = [
            "Absinth",
            "Bier",
            "Brandy",
            "Cachaca",
            "Cognac",
            "Gin",
            "Grappa",
            "Korn",
            "Likör",
            "Pils",
            "Rum",
            "Sherry",
            "Tequila",
            "Vodka",
            "Wein",
            "Kurze",
            "Hubis",
            "Jack Daniels",
            "Wasser"
        ]

        self.__games = [
            "Mach ma ein paar looser in Cyber Hunter platt",
            "Ich glaube, deine Tiere brauchen dich auf deinem Stardew Valley Hof",
            "Spiel The Forest und bau dir ein schönes Haus",
            "Erkunde eine neue Welt in Minecraft",
            "Rette Cloe und lass Acardia Bay untergehen (Wenn nicht, bingt dich Gooser um)",
            "Flirte mit Amber :)",
            "Rette dein Königreich und suche dir ein paar schöne Typen aus",
            "Lebe ein neues Leben in BitLife"
            """Sei ma so richtig sus, lass dich aber nicht erwischen ;)
AMOGUS⣟⣫⡾⠛⠛⠛⠛⠛⠛⠿⣾⣽⡻⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⡟⣼⠏⠀⠀⠀⠀⠀⠀⣀⣀⡀⣙⣿⣎⢿⣿⣿⣿
⣿⣿⣿⣿⣿⢹⡟⠀⠀⠀⣰⡾⠟⠛⠛⠛⠛⠛⠛⠿⣮⡻⣿⣿
⣿⡿⢟⣻⣟⣽⠇⠀⠀⠀⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⢿⡹⣿
⡟⣼⡟⠉⠉⣿⠀⠀⠀⠀⢿⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⣼⢟⣿
⣇⣿⠁⠀⠀⣿⠀⠀⠀⠀⠘⢿⣦⣄⣀⣀⣀⣀⣤⡴⣾⣏⣾⣿
⡇⣿⠀⠀⠀⣿⠀⠀⠀⠀⠀⠀⠈⠉⠛⠋⠉⠉⠀⠀⢻⣿⣿⣿
⢃⣿⠀⠀⠀⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣧⣿⣿
⡻⣿⠀⠀⠀⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣸⣧⣿⣿
⡇⣿⠀⠀⠀⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⢹⣿⣿
⣿⡸⢷⣤⣤⣿⡀⠀⠀⠀⠀⢠⣤⣄⣀⣀⣀⠀⠀⢠⣿⣿⣿⣿
⣿⣿⣷⣿⣷⣿⡇⠀⠀⠀⠀⢸⡏⡍⣿⡏⠀⠀⠀⢸⡏⣿⣿⣿
⣿⣿⣿⣿⣿⢼⡇⠀⠀⠀⠀⣸⡇⣷⣻⣆⣀⣀⣀⣼⣻⣿⣿⣿
            """
        ]

        self.__dices = [
            "⚀",
            "⚁",
            "⚂",
            "⚃",
            "⚄",
            "⚅"
        ]

        self.__diceMeaning = {
            "⚀": 1,
            "⚁": 2,
            "⚂": 3,
            "⚃": 4,
            "⚄": 5,
            "⚅": 6
        }

    def getRandomGame(self):
        return random.choice(self.__games)

    def getRandomActivity(self):
        return random.choice(self.__activitys)

    def getRandomDrink(self):
        return random.choice(self.__drinks)

    def getRandomDice(self, amount):
        value = " | "
        count = 0
        amount = int(amount)
        worth = 0
        while count < amount:
            thisDice = random.choice(self.__dices)
            value = value + thisDice  + " | "
            worth = worth + self.__diceMeaning[thisDice]
            count = count +1
        return value, worth

    def getRandomCatPic(self):
        with open(file="media.json", mode="r") as file:
            media = json.loads(file.read())
            file.close()
        return f"https://gitlab.com/HendrikHeine/discordbot/-/raw/main/media/cats/{random.choice(media['cats'])}"
        