import os
import discord
import logging
import datetime
import secretHandler
import addons as ad
from discord import Status
from discord import Member
from discord import app_commands
from discord.activity import Game
from discord.ext import commands
from discord.ext.commands import Context
from discord.ext.commands import has_permissions
from discord.ext.commands import CheckFailure

secret = secretHandler.secret()
token = secret.loadSecret("secret.txt")
if token[0] != 0:
    exit(f"Cannot load token. Error Code {token[0]}")
botToken = token[1]


addon = ad.addon()

logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='data/log/discord.log', encoding='utf-8', mode='a')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

logger.info("Start up")

botDescription="SussyBot"
intents = discord.Intents.all()
client = commands.Bot(command_prefix="?", help_command=None, description=botDescription, intents=intents)

def changeServerCount(count):
    return Game(name=f"Flirt Rachel Amber |{count}|", start=datetime.datetime.now())

@client.event
async def on_ready():
    logger.info(f"Bot: {client.user.name}")
    logger.info(f"BotID: {client.user.id}")
    countGuilds = 0
    async for guild in client.fetch_guilds():
        countGuilds += 1
    presence = changeServerCount(count=countGuilds)
    await client.change_presence(activity=presence, status=Status.online)

@client.event
async def on_message(ctx:Context):
    await client.process_commands(ctx)
    countGuilds = 0
    async for guild in client.fetch_guilds():
        countGuilds += 1
    presence = changeServerCount(count=countGuilds)
    await client.change_presence(activity=presence, status=Status.online)


@client.command(pass_context=True)
async def getBadges(ctx:Context, user: discord.Member):
    logger.info("Command: getBadges")
    userFlags = user.public_flags.all()
    userBadges = []
    logger.info(user.premium_since)
    if user.premium_since != None:userBadges.append("discord_nitro")
    for flag in userFlags:
        logger.info(flag.name)
        userBadges.append(flag.name)
    await ctx.send(content=userBadges)

@client.command()
async def games(ctx:Context):
    logger.info("Command: games")
    await ctx.reply(addon.getRandomGame())

@client.command()
async def saufen(ctx:Context):
    logger.info("Command: saufen")
    await ctx.reply(f"Sauf {addon.getRandomDrink()}")

@client.command()
@has_permissions(administrator=True)
async def clear(ctx:Context, realy=""):
    logger.info("Command: clear")
    if realy == "YES":
        await ctx.channel.purge()
    else:
        await ctx.channel.send("Wenn du wirklich ALLE Nachrichten in diesem Text-Kanal löschen willst, schreib ?clear YES")

@clear.error
async def clear_error(ctx:Context, error):
    if isinstance(error, CheckFailure):
        await ctx.reply("👀Du bist kein Admin🤌")

@client.command()
async def helpMe(ctx:Context):
    logger.info("Command: helpMe")
    guild = ctx.guild
    guild:discord.Guild

    embed = discord.Embed(title="Help", timestamp=datetime.datetime.utcnow(), color=discord.Color.blue())
    embed.set_thumbnail(url=f"https://cdn.discordapp.com/icons/{guild.id}/{guild.icon}.png")
    embed.add_field(name="?saufen", value="Hol dir n Drink", inline=False)
    embed.add_field(name="?games", value="Gib mir ein Spiel zum zocken, ich kann mich nicht entscheiden...", inline=False)
    embed.add_field(name="?cat", value="Random Cat Memes :D", inline=False)
    embed.add_field(name="?info", value="Bekomme ein paar Infos zum Server", inline=False)
    embed.add_field(name="?roleDice <amount>", value="Würfle eine Anzahl an Würfel", inline=False)
    embed.add_field(name="?diceMatch <@PLAYER_ONE> <@PLAYER_TWO> <amount>", value="Macht ein Würfel match zwischen 2 Spielern", inline=False)
    embed.add_field(name="?excuse", value="Eine zufällige Ausrede von programmingexcuses.com", inline=False)
    embed.add_field(name="?getBadges <@USER>", value="Holt alle Badges aus dem Profil des users", inline=False)

    #this should only append if user is admin
    embed.add_field(name="?getAvatar <@USER>", value="Zeige Profilbild", inline=False)
    embed.add_field(name="?clear", value="Löscht alle Nachrichten des Channels", inline=False)

    await ctx.send(embed=embed)

@client.command()
async def roleDice(ctx:Context, amount):
    logger.info("Command: roleDice")
    thisDice, worth = addon.getRandomDice(amount)
    await ctx.reply(f"{thisDice} \n{worth}")

@client.command()
async def diceMatch(ctx:Context, playerOne, playerTwo, amount:int):
    logger.info("Command: diceMatch")
    guild = ctx.guild
    guild:discord.Guild

    playerOne = playerOne[2:]
    playerOne = int(playerOne[:-1])
    playerOne = guild.get_member(playerOne)

    playerTwo = playerTwo[2:]
    playerTwo = int(playerTwo[:-1])
    playerTwo = guild.get_member(playerTwo)

    diceForPlayerOne, worthForPlayerOne = addon.getRandomDice(amount)
    diceForPlayerTwo, worthForPlayerTwo = addon.getRandomDice(amount)
    winner = "Nobody"
    if worthForPlayerOne < worthForPlayerTwo:winner = playerTwo
    elif worthForPlayerOne > worthForPlayerTwo:winner = playerOne
    else:pass

    embed = discord.Embed(title="DiceMatch", color=discord.Color.blue())
    embed.add_field(name=str(playerOne), value=f"{diceForPlayerOne}\n{worthForPlayerOne}")
    embed.add_field(name=str(playerTwo), value=f"{diceForPlayerTwo}\n{worthForPlayerTwo}")
    embed.add_field(name="Winner", value=f"{winner}")
    await ctx.send(embed=embed)

@client.command()
async def birthday(ctx:Context, user):
    logger.info("Command: birthday")
    if "@" in user:
        await ctx.reply(content=f"Happy Birthday {user} 🥳 <3")
    else:
        await ctx.reply("Mention a user please :)")

@client.command()
async def meanwhile(ctx:Context):
    logger.info("Command: meanwhile")
    await ctx.reply(addon.getRandomActivity())

@client.command()
async def info(ctx:Context):
    logger.info("Command: info")
    guild = ctx.guild
    guild:discord.Guild
    embed = discord.Embed(title=f"{guild.name}", description="about this Server", timestamp=datetime.datetime.utcnow(), color=discord.Color.blue())
    embed.add_field(name="Bot Quellcode", value="https://gitlab.com/HendrikHeine/discordbot")
    await ctx.send(embed=embed)

@client.command()
async def cat(ctx:Context):
    logger.info("Command: cat")
    value = addon.getRandomCatPic()
    embed = discord.Embed(title="Catto^.^", color=discord.Color.blue())
    embed.set_image(url=value)
    await ctx.reply(embed=embed)

@client.command()
@has_permissions(administrator=True)
async def getAvatar(ctx:Context, userID):
    logger.info("Command: getAvatar")
    guild = ctx.guild
    guild:discord.Guild
    userID = userID[2:]
    userID = int(userID[:-1])
    user:Member = guild.get_member(userID)
    avatarURL = f"https://cdn.discordapp.com/avatars/{user.id}/{user.avatar}.png"
    await ctx.reply(content=avatarURL)

@getAvatar.error
async def getAvatar_error(ctx:Context, error):
    if isinstance(error, CheckFailure):
        await ctx.reply("👀Du bist kein Admin🤌")

client.run(botToken)