import os
import json

mediaCats = os.listdir("media/cats")

media = {
    "cats": mediaCats
}

with open(file="media.json", mode="w") as file:
    file.write(json.dumps(obj=media, indent=2))
    file.close()
