FROM python:3.11-alpine

WORKDIR /app
COPY requirements.txt requirements.txt
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip3 install -r requirements.txt
RUN rm requirements.txt

COPY ./src/ .
COPY ./media ./media
RUN python3 prepareDocker.py
RUN rm -rf ./media
RUN rm prepareDocker.py
RUN mkdir ./data
RUN mkdir ./data/log
RUN mkdir ./data/config

CMD [ "python3", "main.py" ]
